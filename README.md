# GWAS-cookbook-V2-2021-11-28-data

#### 介绍

这是我编写的GWAS-cookbook-V2-2021-11-28 pdf的配套数据。原数据中data1来源于github（https://github.com/MareesAT/GWA_tutorial/）

但是代码中有些R语言有bug，而且github下载东西很慢。所以我把所有的数据+代码放到我的gitee上，方便大家下载使用。如果大家有疑问，可以关注公众号“育种数据分析之放飞自我”进行咨询。

